function loadText(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
    if(xhr.status === 200)
        return xhr.responseText;
    else {
        return null;
    }
}

// variables globales du programme;
var canvas;
var gl; //contexte
var program; //shader program
var attribPos; //attribute position
var attribSize; //attribute size
var pointSize = 10;

function initContext(){
    canvas = document.getElementById('dawin-webgl');
    gl = canvas.getContext('webgl');
    if (!gl) {
        console.log('ERREUR : echec chargement du contexte');
        return;
    }
    gl.clearColor(0.6, 0.2, 0.2, 1.0);
}

//Initialisation des shaders et du program
function initShaders() {
  var ShaderSource = loadText('shader.glsl');
  var shader = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(shader, ShaderSource);
  gl.compileShader(shader);
  if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)==false) {
    console.log(gl.getShaderInfoLog(shader));
  }
  var ShaderSourceF = loadText('fragment.glsl');
  var shaderF = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(shaderF, ShaderSourceF);
  gl.compileShader(shaderF);
  gl.getShaderParameter(shaderF, gl.COMPILE_STATUS);
  if (gl.getShaderParameter(shaderF, gl.COMPILE_STATUS)==false) {
    console.log(gl.getShaderInfoLog(shaderF));
  }
  program = gl.createProgram();


  gl.attachShader(program, shader);
  gl.attachShader(program, shaderF);
  gl.linkProgram(program);
  if (gl.getProgramParameter(program, gl.LINK_STATUS)==false) {
    console.log(gl.getProgramInfoLog(program));
  }
  gl.getProgramParameter(program, gl.LINK_STATUS);
  gl.getProgramInfoLog (program)

  gl.useProgram(program);
}

//Fonction initialisant les attributs pour l'affichage (position et taille)
function initAttributes() {
  var pointerPos = gl.getAttribLocation(program,"position");

convas.oncliick = function(e) {
    posX = event.clientX;
    posY= event.clientY;
    console.log(posX+""+posY);
    gl.vertexAttrib2f(pointerPos,posX/400 -1, -(posY/400)-1));
    gl.drawArrays(gl.POINTS, O, 1);
}

  for (var i = -0.90; i < 1; i+=0.2) {
    for (var y = -0.90; y < 1; y+=0.2) {
      gl.vertexAttrib2f(pointerPos,i,y);
      gl.drawArrays(gl.POINTS, 0, 1);
    }

  }


}

//Fonction permettant le dessin dans le canvas
function draw() {
  gl.drawArrays(gl.POINTS, 0, 1);
}


function main() {
    initContext();
    initShaders();
    gl.clear(gl.COLOR_BUFFER_BIT);
    initAttributes();
}