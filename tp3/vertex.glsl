attribute vec4 position;
attribute vec4 color;
varying vec4 vcolor;
void main() {
    float x_r = position[0] * cos(angle) - position[1] * sin(angle);
    float y_r = position[0] * sin(angle) + position[1] * cos(angle);
    gl_Position = vec4(x_r*zoom+translation[0],y_r*zoom+translation[1],position[2],position[3]);
    gl_PointSize = (position[0] + 1.0) * 20.0;
    vcolor = color;
}