function loadText(url) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, false);
  xhr.overrideMimeType("text/plain");
  xhr.send(null);
  if(xhr.status === 200)
  return xhr.responseText;
  else {
    return null;
  }
}

// variables globales du programme;
var canvas;
var gl; //contexte
var program; //shader program
var attribPos; //attribute position
var pointSize = 10.;
var triangleSize = 1;
var rotaX = 1;
var rotaY = 1;
var mousePositions = [ ];
var buffer;
var tx=0;
var ty=0;
var angle = 20;
var angleEnRadians = 0;
var start=null;
var dirX = "right";
var dirY = "up";
var vitesseX = 0.01;
var vitesseY = 0.01;


var posTriangle = [-0.1, 0.1, 0.1, 0.1, 0.0, -0.1];
var posSquare = [-0.1, -0.1,
  -0.1, 0.1,
  0.1, -0.1,
  0.1, 0.1,
  0.1, 0.1,
  0.1, -0.1];
  var posTriangleMat = [];

  var colors = [];
  const colorsTriangle = [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random()];

  function initContext() {
    canvas = document.getElementById('dawin-webgl');
    gl = canvas.getContext('webgl');
    if (!gl) {
      console.log('ERREUR : echec chargement du contexte');
      return;
    }
    gl.clearColor(0.2, 0.2, 0.2, 1.0);
  }

  //Initialisation des shaders et du program
  function initShaders() {
    var fragmentSource = loadText('fragment.glsl');
    var vertexSource = loadText('vertex.glsl');

    var fragment = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragment, fragmentSource);
    gl.compileShader(fragment);

    var vertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertex, vertexSource);
    gl.compileShader(vertex);

    gl.getShaderParameter(fragment, gl.COMPILE_STATUS);
    gl.getShaderParameter(vertex, gl.COMPILE_STATUS);

    if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS)) {
      console.log(gl.getShaderInfoLog(fragment));
    }

    if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS)) {
      console.log(gl.getShaderInfoLog(vertex));
    }

    program = gl.createProgram();
    gl.attachShader(program, fragment);
    gl.attachShader(program, vertex);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      console.log("Could not initialise shaders");
    }
    gl.useProgram(program);
  }



  //Evenement souris
  function initEvents() {
    canvas.onclick = function(e) {
      //changement de repere pour les coordonnees de souris
      var x = (e.pageX/canvas.width)*2.0 - 1.0;
      var y = ((canvas.height-e.pageY)/canvas.height)*2.0 - 1.0;
      mousePositions.push(x);
      mousePositions.push(y);
      draw();
    }

    document.onkeydown = function(e){
      // console.log(e.key);
      switch (e.key) {
        case "ArrowRight":
        tx += 0.1;
        break;
        case "ArrowLeft":
        tx -= 0.1
        break;
        case "ArrowDown":
        ty -= 0.1;
        break;
        case "ArrowUp":
        ty += 0.1;
        break;
        case "z":
        triangleSize += 0.1;
        break;
        case "s":
        triangleSize -= 0.1;
        break;
        case "q":
        angleEnRadians = angle * Math.PI / 180;
        angle +=5;
        break;
        case "d":
        angleEnRadians = angle * Math.PI / 180;
        angle -=5;
        break;
        default:

      }
      // draw();

    }
  }

  //TODO
  //Fonction initialisant les attributs pour l'affichage (position et taille)
  function initAttributes() {
    attribPos = gl.getAttribLocation(program, "position");
    attribColor = gl.getAttribLocation(program, "color");
    attribTranslation = gl.getUniformLocation(program, "translation");
    attribSize = gl.getAttribLocation(program, "size");
    attributeRota = gl.getUniformLocation(program, "rotation");
    for (var i = 0; i < 9; i++) {
      for (var j = 0; j < 9; j++) {
        posTriangleMat.push(-0.92 + 0.2 *i);
        posTriangleMat.push(0.9 - 0.2 *j);

        posTriangleMat.push(-0.72 + 0.2 *i);
        posTriangleMat.push(0.9 - 0.2 *j);

        posTriangleMat.push(-0.82 + 0.2 *i);
        posTriangleMat.push(0.7 - 0.2 *j);

        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);

        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);

        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);
      }
    }
  }


  //TODO
  //Initialisation des buffers
  function initBuffers() {
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mousePositions),gl.STATIC_DRAW);

    gl.enableVertexAttribArray(attribPos);
    gl.vertexAttribPointer(attribPos, 2, gl.FLOAT, true, 0, 0)

    gl.drawArrays(gl.LINE_LOOP, 0, mousePositions.length/2);

    // console.log(mousePositions);
  }

  function initBuffersTriangle() {
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(posTriangle),gl.STATIC_DRAW);

    gl.enableVertexAttribArray(attribPos);
    gl.vertexAttribPointer(attribPos, 2, gl.FLOAT, true, 0, 0)

    gl.drawArrays(gl.TRIANGLES, 0, posTriangle.length/2);

    gl.uniform2f(attribTranslation, tx, ty);
    gl.vertexAttrib1f(attribSize, triangleSize);
    gl.uniform1f(attributeRota, angleEnRadians);


    var bufferColor = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colorsTriangle),gl.STATIC_DRAW);

    gl.enableVertexAttribArray(attribColor);
    gl.vertexAttribPointer(attribColor, 2, gl.FLOAT, true, 0, 0)

    gl.drawArrays(gl.TRIANGLES, 0, colorsTriangle.length/2);

    // console.log( posTriangle);

  }

  function refreshTriangle(){
    gl.uniform2f(attribTranslation, tx, ty);
    gl.vertexAttrib1f(attribSize, triangleSize);
    gl.uniform1f(attributeRota, angleEnRadians);
    gl.vertexAttribPointer(attribColor, 2, gl.FLOAT, true, 0, 0)
    gl.drawArrays(gl.TRIANGLES, 0, colorsTriangle.length/2);

    function step(timestamp) {
  var progress;
  if (start === null) start = timestamp;
  progress = timestamp - start;
  angleEnRadians = angle * Math.PI / 180;

  if(tx > 1)
  {
    dirX = "left";
    vitesseX = (Math.random() * 0.02)+0.01;
  }
  if(tx < -1)
  {
    dirX = "right";
    vitesseX = (Math.random() * 0.02)+0.01;
  }
  if(ty > 1)
  {
    dirY = "up";
    vitesseY = (Math.random() * 0.02)+0.01;
  }
  if(ty < -1)
  {
    dirY = "down";
    vitesseY = (Math.random() * 0.02)+0.01;
  }

  switch (dirX) {
    case "left":
    tx -= vitesseX;
      break;
    case "right":
    tx += vitesseX;
      break;
    default:

  }
  switch (dirY) {
    case "up":
    ty -= vitesseY;
      break;
    case "down":
    ty += vitesseY;
      break;
    default:

  }
  angle += 1;
  // colorsTriangle = []
  // colorsTriangle = [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random()];
  draw();
}
requestAnimationFrame(step);
  }

  function refreshMatTriangle(){
    gl.uniform2f(attribTranslation, tx, ty);
    gl.vertexAttrib1f(attribSize, triangleSize);
    gl.uniform1f(attributeRota, angleEnRadians);
    gl.vertexAttribPointer(attribColor, 2, gl.FLOAT, true, 0, 0)
    gl.drawArrays(gl.TRIANGLES, 0, colors.length/4);
  }

  function initBuffersMatrixTriangle() {
    var buffer = gl.createBuffer();
    var bufferColor = gl.createBuffer();

    // [-0.9, 0.9, -0.7, 0.9, -0.8, 0.7]
    for (var i = 0; i < 9; i++) {
      for (var j = 0; j < 9; j++) {
        posTriangleMat.push(-0.92 + 0.2 *i);
        posTriangleMat.push(0.9 - 0.2 *j);

        posTriangleMat.push(-0.72 + 0.2 *i);
        posTriangleMat.push(0.9 - 0.2 *j);

        posTriangleMat.push(-0.82 + 0.2 *i);
        posTriangleMat.push(0.7 - 0.2 *j);
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);

        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);

        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(Math.random());
        colors.push(1);

      }
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(posTriangleMat),gl.STATIC_DRAW);

    gl.enableVertexAttribArray(attribPos);
    gl.vertexAttribPointer(attribPos, 2, gl.FLOAT, true, 0, 0)
    gl.drawArrays(gl.TRIANGLES, 0, posTriangleMat.length/2);


    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors),gl.STATIC_DRAW);

    gl.uniform2f(attribTranslation, tx, ty);
    gl.vertexAttrib1f(attribSize, triangleSize);
    gl.uniform1f(attributeRota, angleEnRadians);


    gl.enableVertexAttribArray(attribColor);
    gl.vertexAttribPointer(attribColor, 4, gl.FLOAT, true, 0, 0)

    gl.drawArrays(gl.TRIANGLES, 0, colors.length/4);

    // console.log(colors);
    // console.log(posTriangleMat);
  }

  function initBuffersSquare() {
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(posSquare),gl.STATIC_DRAW);

    gl.enableVertexAttribArray(attribPos);
    gl.vertexAttribPointer(attribPos, 2, gl.FLOAT, true, 0, 0)

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, posSquare.length/2);
    // console.log(posSquare);
  }

  //TODO
  //Mise a jour des buffers : necessaire car les coordonnees des points sont ajoutees a chaque clic
  function refreshBuffers() {
    // initBuffers();
    // initBuffersTriangle();
    // initBuffersMatrixTriangle();
    // initBuffersSquare();
    // refreshMatTriangle();
    refreshTriangle();
  }

  //TODO
  //Fonction permettant le dessin dans le canvas
  function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    refreshBuffers();
  }



  function main() {
    initContext();
    initShaders();
    initAttributes();
    // initBuffers();
    initBuffersTriangle();
    // initBuffersMatrixTriangle();
    initEvents();

    draw();
  }
